#!/usr/bin/env bash
PIDFN=/tmp/pamlogUpdater.pid
if kill $(cat $PIDFN) >/dev/null 2>&1
then
    printf "pamlogUpdater stopped\n"
    exit 0
else
    printf "no pamlogUpdater to stop my friend\n"
    exit 1
fi
