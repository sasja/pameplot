#!/usr/bin/env bash
PIDFN=/tmp/pamlogUpdater.pid
if ps -p $(cat $PIDFN) >/dev/null 2>&1
then
    printf "seems pamlogUpdater is already running my friend\n"
    exit 1
else
    printf "starting loop to update /root/pameplot/pamela.log\n"
    (
        echo $BASHPID > $PIDFN
        cd /root/pameplot
        while true
        do
            date
            printf "appending pamela snapshot to log\n"
            ./pamdb2json.py | ./rotlog2 pamela 100000
            sleep 60
        done 2>&1 | logger -t pamlogUpdater
    ) &
    exit 0
fi
