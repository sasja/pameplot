#!/usr/bin/env bash
PIDFN=/tmp/pameplot.pid
if kill $(cat $PIDFN) >/dev/null 2>&1
then
    printf "pameplot stopped\n"
    exit 0
else
    printf "no pameplot to stop my friend\n"
    exit 1
fi
