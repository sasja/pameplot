#!/usr/bin/env bash
PIDFN=/tmp/enzianSignal.pid
if ps -p $(cat $PIDFN) >/dev/null 2>&1
then
    printf "seems enzianSignal is already running my friend\n"
    exit 1
else
    printf "starting enzianSignal service\n"
    (
        echo $BASHPID > $PIDFN
        cd /root/pameplot
        while true
        do
            date
            ./log2signal.py --ifile=iplog.log --ofile=tmp.signal --totaltime=86400 --dt=200 --type=unique --bintime=4000 && mv tmp.signal /var/www/html/pam/iplog.signal
            ./log2signal.py --ifile=chauffage.log --ofile=tmp.signal --totaltime=86400 --dt=200 --type=mean --bintime=1000 && mv tmp.signal /var/www/html/pam/chauffage.signal
            ./log2signal.py --ifile=revbank.log --ofile=tmp.signal --totaltime=86400 --dt=500 --type=count --bintime=4000 && mv tmp.signal /var/www/html/pam/revbank.signal
            ./log2signal.py --ifile=pamela.log --ofile=tmp.signal --totaltime=86400 --dt=1000 --type=unique --bintime=2000 && mv tmp.signal /var/www/html/pam/pamela.signal
            sleep 300
        done 2>&1 | logger -t pameplot
    ) &
    exit 0
fi
