#!/usr/bin/env bash
PIDFN=/tmp/pameplot.pid
if ps -p $(cat $PIDFN) >/dev/null 2>&1
then
    printf "seems pameplot is already running my friend\n"
    exit 1
else
    printf "starting pameplot service\n"
    (
        echo $BASHPID > $PIDFN
        cd /root/pameplot
        while true
        do
            date
            ./updateLogs.sh
            ./pameplot.py pameplot.conf && mv pameplot.png /var/www/html/pam/pameplot.png
            ./pameplot.py pameplot_short.conf && mv pameplot_short.png /var/www/html/pam/pameplot_short.png
            sleep 300
        done 2>&1 | logger -t pameplot
    ) &
    exit 0
fi
