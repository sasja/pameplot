#!/usr/bin/env python
from __future__ import print_function
import json
import time, datetime
import sys
import getopt

day_s = 60 * 60 * 24
typestrings=["count","unique","sum","mean","min","max"]

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def main(argv):
    usage = "usage: " + argv[0] + " --ifile= --ofile= --totaltime= --dt= --type= --bintime="
    neededlongopts=["ifile=","ofile=","totaltime=","dt=","type=","bintime="]
    try:
        opts, args = getopt.getopt(argv[1:],"h",neededlongopts)
    except getopt.GetoptError:
        eprint(usage)
        sys.exit(1)
    if any([("--"+opt.strip("=")) not in [opt[0] for opt in opts] for opt in neededlongopts]):
        eprint(usage)
        sys.exit(1)
    for opt, arg in opts:
        if opt == '-h':
            eprint(usage)
            sys.exit()
        elif opt=="--ifile":
            inputfile = arg
        elif opt=="--ofile":
            outputfile = arg
        elif opt=="--totaltime":
            try:
                totaltime=int(arg)
            except ValueError:
                eprint("--totaltime must be an integer value in seconds")
                eprint(usage)
                sys.exit(1)
        elif opt=="--dt":
            try:
                dt=int(arg)
            except ValueError:
                eprint("--dt must be an integer value in seconds")
                eprint(usage)
                sys.exit(1)
        elif opt=="--type":
            if arg not in typestrings:
                eprint("--type must be one of \"" + ", ".join(typestrings) + "\"")
                eprint(usage)
                sys.exit(1)
            else:
                signaltype = arg
        elif opt=="--bintime":
            try:
                bintime=int(arg)
            except ValueError:
                eprint("--bintime must be an integer value in seconds")
                eprint(usage)
                sys.exit(1)
    signal = getSignal(inputfile, totaltime, dt, signaltype, bintime)
    eprint("writing " + outputfile)
    with open(outputfile, "w") as outfile:
        json.dump(signal, outfile)

def getSignal(inputfile, totaltime, dt, signaltype, bintime, nodatavalue=0):
    now = time.time()
    nBins = int(totaltime / dt)
    startT = now - nBins * dt + dt - bintime
    signalStartT = now - nBins * dt
    counts = [0] * nBins
    sums = [0] * nBins
    uniques = [set() for i in range(nBins)]
    mins = [None] * nBins
    maxs = [None] * nBins
    eprint("reading " + inputfile)
    with open(inputfile) as log:
        for line in log:
            timestamp, eventstring = json.loads(line)
            # skip lines until reaching first bin
            # TODO there is some seriously fucked up code here, but it works
            if (timestamp + bintime) <= startT:
                continue
            #calc nr of first/last bin possibly affected
            firstbin = max(0,int((timestamp - signalStartT) / dt))
            lastbin = min(nBins-1,int((timestamp - startT) / dt))
            for binnr in range(firstbin, lastbin + 1):
                binmin = startT + binnr * dt
                binmax = binmin + bintime
                if binmin<timestamp<binmax:
                    if signaltype in ["count","mean"]:
                        counts[binnr] += 1
                    if signaltype in ["mean","sum"]:
                        sums[binnr] += float(eventstring)
                    elif signaltype == "unique":
                        uniques[binnr].add(eventstring)
                    elif signaltype == "min":
                        val = float(eventstring)
                        if (mins[binnr] == None) or (val < mins[binnr]):
                            mins[binnr] = val
                    elif signaltype == "max":
                        val = float(eventstring)
                        if (maxs[binnr] == None) or (val > maxs[binnr]):
                            maxs[binnr] = val
    if signaltype == "count":
        values = counts
    if signaltype == "unique":
        values = [len(a) for a in uniques]
    if signaltype == "sum":
        values = sums
    if signaltype == "mean":
        values = [sums[n]/counts[n] if counts[n] else None for n in range(len(sums))]
    if signaltype == "min":
        values = mins
    if signaltype == "max":
        values = maxs
    signal = [(now - (nBins - n - 1) * dt, value if value != None else nodatavalue) for n,value in enumerate(values)]
    return signal

if __name__ == "__main__":
    main(sys.argv)
