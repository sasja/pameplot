#!/usr/bin/env python
from __future__ import print_function
import sys
import json
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import log2signal
import time
import datetime

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def main(argv):
    usage = "usage: " + argv[0] + " plot.conf"
    if len(argv) != 2:
        eprint(usage)
        sys.exit(1)
    configFilename = argv[1]
    eprint("loading configuration file: " + configFilename)
    try:
        with open(configFilename) as f:
            conf = json.load(f)
    except:
        eprint(usage)
        eprint("could not open and parse config file: " + argv[1])
        sys.exit(1)
    handleConf(conf)

def handleConf(conf):
    #eprint(conf)
    outputfile = conf["outputfile"]
    width = conf["width"]
    height = conf["height"]
    totaltime = conf["totaltime"]
    dpi = conf.get("dpi", 300)
    ticks = conf.get("ticks", None)
    data = conf["data"]
    plt.figure(figsize=(float(width)/dpi, float(height)/dpi), dpi=dpi)
    minx, maxx = None, None
    miny, maxy = None, None
    for item in data:
        inputfile = item["input"]
        color = item["color"]
        thickness = item["thickness"]
        bintime = item["bintime"]
        dt = item["dt"]
        signaltype = item["type"]
        scale = item.get("scale",None)
        nodatavalue = item.get("nodatavalue",0)
        signal = log2signal.getSignal(inputfile, totaltime, dt, signaltype, bintime, nodatavalue)
        x,y = zip(*signal)
        if scale == "auto":
            maximum = float(max(y))
            minimum = float(min(y))
            dy = max(y) - min(y)
            if dy > 0:
                y = [(v-minimum)/dy for v in y]
            else:
                y = [0 for v in y]
        plt.plot(x,y, lw=thickness, color=color)
        if maxx == None:
            maxx = max(x)
        else:
            maxx = max(maxx, max(x))
        if minx == None:
            minx = min(x)
        else:
            minx = min(minx, min(x))
        if maxy == None:
            maxy = max(y)
        else:
            maxy = max(maxy, max(y))
        if miny == None:
            miny = min(y)
        else:
            miny = min(miny, min(y))
    dx = maxx - minx
    dy = maxy - miny
    if ticks == "days":
        nTicks = int(totaltime/(60*60*24))
        lastMidnight = time.mktime(datetime.date.today().timetuple())
        xTicks = [(lastMidnight - (nTicks - 1 - i)*60*60*24) for i in range(nTicks)]
        plt.xticks(xTicks,[])
    elif ticks == "hours":
        nTicks = int(totaltime/(60*60))
        lastMidnight = time.mktime(datetime.date.today().timetuple())
        xTicks = [(lastMidnight - (nTicks - 1 - i)*60*60) for i in range(2*nTicks)]
        plt.xticks(xTicks,[])
    else:
        plt.xticks([])
    plt.yticks([])
    plt.axis([minx-dx*0.005, maxx+dx*0.005, miny-dy*0.005, maxy+dy*0.005])
    plt.box(on="off")
    plt.grid(True)
    eprint("saving plot to " + outputfile)
    plt.savefig(outputfile, bbox_inches='tight', pad_inches = 0)
        
if __name__ == "__main__":
    main(sys.argv)
