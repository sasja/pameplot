#!/usr/bin/env bash

# simple circular buffer logger
# it reads lines from stdin, and appends to a file
# 
# usage:
#
#   rotlog2 name [MINLINES]
#
# name          name of the log, .log will be appended
# MINLINES      number of lines to store at least
#
# okt 2016, sasja

FN_HEAD=${1:-rotlog}
MINLINES=${2:-5000}

TRIGGERMAX=$((MINLINES * 6 / 5))

FN="$FN_HEAD".log
LOCK=."$FN_HEAD".lock

while read -r line; do
    # try and get lock, timeout after 10s
    (
        flock -w 10 200 || {
            echo "ERROR: could not obtain lock in time!" 1>&2;
            exit 1;
        }
        printf "%s\n" "$line" >> $FN
        if [[ $(wc -l <$FN) -gt $TRIGGERMAX ]]; then
            tmpfn="$FN_HEAD".tmp
            tail -n $MINLINES $FN > $tmpfn
            mv $tmpfn $FN
        fi
    ) 200>$LOCK
done
