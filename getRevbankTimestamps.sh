#!/usr/bin/env bash
gitPath=/root/pameplot/revbankwhitespace

cd $gitPath
git pull 1>/dev/nul 2>&1
git log --since="$(date --date='now - 6 months')" --pretty=%ad --date=rfc \
    | date -f - +%s \
    | while read timestamp
    do
        printf "[%s,\"transaction\"]\n" "$timestamp"
    done
