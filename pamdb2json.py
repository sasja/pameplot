#!/usr/bin/env python
import sqlite3
import json

PAMDB = "/var/www/html/pam/db/pamela.sql"

conn = sqlite3.connect(PAMDB)
c = conn.cursor()
c.execute("select committime, data from data")
result = c.fetchall()
conn.close()

for item in result:
    print json.dumps(item)
