#!/usr/bin/env bash

# simple circular buffer logger
# it reads lines from stdin, and appends json arrays to a file
# [timestamp, "content of the input line"]
# the timestamp is seconds the epoch
# 
# usage:
#
#   rotlog name [MINLINES]
#
# name          name of the log, .log will be appended
# MINLINES      number of lines to store at least
#
# sept 2016, sasja

FN_HEAD=${1:-rotlog}
MINLINES=${2:-5000}

TRIGGERMAX=$((MINLINES * 6 / 5))

FN="$FN_HEAD".log
LOCK=."$FN_HEAD".lock

while read -r line; do
    # escaping \ and " for json
    escaped=$(printf "%s" "$line" | sed -e 's/\\/\\\\/' | sed -e 's/\"/\\\"/')
    timestamp=$(date +%s)
    # try and get lock, timeout after 10s
    (
        flock -w 10 200 || {
            echo "ERROR: could not obtain lock in time!" 1>&2;
            exit 1;
        }
        printf "[%s,\"%s\"]\n" "$timestamp" "$escaped" >> $FN
        if [[ $(wc -l <$FN) -gt $TRIGGERMAX ]]; then
            tmpfn="$FN_HEAD".tmp
            tail -n $MINLINES $FN > $tmpfn
            mv $tmpfn $FN
        fi
    ) 200>$LOCK
done

